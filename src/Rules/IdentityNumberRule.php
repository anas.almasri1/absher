<?php

namespace aqsat_integration_kyc\absher\Rules;

use Illuminate\Contracts\Validation\Rule;

class IdentityNumberRule implements Rule{

    /**
     * Determine if the validation rule passes.
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes( $attribute , $value ){

        return true; // todo check card number format

    }

    /**
     * Get the validation error message.
     * @return string
     */
    public function message(){

        return trans('helper::response.invalid_identity_number');

    }
}
