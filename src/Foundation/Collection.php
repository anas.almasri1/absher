<?php

namespace aqsat_integration_kyc\absher\Foundation;

use aqsat\kyc\Models\CountryKycConfig;

class Collection {

    public static function getCountryKycConfig(){

        $uuid = config('kyc.country_kyc_uuid');

        $country_kyc_config = CountryKycConfig::getWhereIn('country_kyc_uuid' , $uuid);

        $final = [];

        foreach ($country_kyc_config as $row) {

            $final[$row->key] = $row->value;

        }

       return $final;

    }


    public static function prepareKycResponse(array $data){

        $validation_array['request_valid']  = Validation::kycForm();
        $validation_array['response_valid']  = Validation::mappingResponse($data);

       return [

                'is_valid' => true, // todo make it dynamic based on absher response

                'acceptable_attributes' => array_keys($validation_array['request_valid']),

                'response_result' => $validation_array['response_valid']

              ];

    }

}
