<?php

namespace aqsat_integration_kyc\absher\Foundation;

use Illuminate\Http\Response;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;
use aqsat_integration_kyc\absher\Facade\Absher;
use aqsat\kyc\Interface\KycInterface;
use aqsat\helper\Foundation\ResponseTemplate;

class ProviderKyc implements KycInterface{


    public function getResult(){


        $rules = Validation::kycValidation();

        $request = request()->all();

        $validator = Validator::make($request,$rules);

        if ($validator->fails()) {

            return [

                'is_valid' => false,

                'errors' => $validator->errors()->toArray()
            ];

        }

        try {

            $response = Absher::absher()->list($request);

            //$response =  json_decode($response->getBody()->getContents() , true);

            if(isset($response['is_valid']) && $response['is_valid'] == 'demo'){
                return $response;
            }
            return Collection::prepareKycResponse($response);

        }catch (ClientException $exception){

            return $exception->getMessage(); // todo add logs

        }
    }

    public function kycForm(){

        return Validation::kycForm();

    }


}
