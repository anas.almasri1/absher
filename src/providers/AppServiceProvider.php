<?php

namespace aqsat_integration_kyc\absher\providers;

use aqsat_integration_kyc\absher\api\Main;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider{

    public function register() {

        if($this->app->runningInConsole()){

            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

            $this->commands([

            ]);
        }


        $this->registerConfig();

    }


    public function boot() {

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'absher');

    }

    protected function registerConfig(){

        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('Test.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'absher'
        );

        $this->app->singleton('Absher', static function () {

            return new Main();
        });
    }
}
