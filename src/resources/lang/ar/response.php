<?php


return [
    'identity_id'=>'الهوية الوطنية',
    'phone'=>'رقم الهاتف',
    'date_of_birth'=>'تاريخ الولادة',

    'demo_check_phone_residence'=>'انتهت مدة الإقامة',
    'demo_check_phone_number_expired'=>'انتهت صلاحية رقم الهاتف',
    'demo_first_name_from_kyc'=>'الاسم الأول من KYC جديد',
    'demo_last_name_from_kyc_new'=>'الاسم الاخير من اعرف عميلك جديد',
];
