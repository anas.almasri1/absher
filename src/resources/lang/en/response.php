<?php


return [
    'identity_id'=>'National ID',
    'phone'=>'Phone Number',
    'date_of_birth'=>'Date of birth',

    'demo_check_phone_residence'=>'The Residence Has Expired',
    'demo_check_phone_number_expired'=>'The Phone Number Expired',
    'demo_first_name_from_kyc'=>'First Name From kyc New',
    'demo_last_name_from_kyc_new'=>'Last Name From KYC New',

];
