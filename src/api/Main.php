<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 */

namespace aqsat_integration_kyc\absher\api;


class Main{

    final public static function absher( $debug = false, $time_out = 30): Absher
    {

        return Absher::object($debug  , $time_out  );
    }


}
