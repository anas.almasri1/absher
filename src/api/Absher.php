<?php

namespace aqsat_integration_kyc\absher\api;

use Psr\Http\Message\ResponseInterface;
use aqsat_integration_kyc\absher\Foundation\Collection;

class Absher{


    private array $endpoint = [
        'absher'=>'absher'
    ];

    private mixed $timeOut;

    private mixed $debug;

    private array $config;


    final public function __construct($debug = false , $timeOut = 30){

        $this->debug = $debug ;

        $this->timeOut = $timeOut;

        $this->config = Collection::getCountryKycConfig();
    }


    final public function list($request)
    {

        /* just for demo to return simulation result */
        switch ($request['phone']) {

            case "966545112340":
                return  [

                                    'is_valid' => 'demo',
                                    'errors' => ['phone'=>[trans('absher::response.demo_check_phone_residence')]]
                ];
                break;

            case "966545112341":
                return  [
                                    'is_valid' => 'demo',
                                    'errors' => ['phone'=>[trans('absher::response.demo_check_phone_number_expired')]]

                ];
                break;

            default:

                switch ($request['phone']) {

                    case "966550001556":
                        $data_otp_kyc =   [

                            'first_name' => 'Fahad',
                            'last_name' => 'Kaled',
                            'identity_id' => '1080053912',
                            'gender' => 'Male',
                            'otp' => '1323',
                            'phone' => '966550001556',
                            'date_of_birth' =>'1411-12-29',
                        ];

                        break;

                    case "966532042041":
                        $data_otp_kyc =   [
                            'first_name' => 'Akram',
                            'last_name' => 'Saud',
                            'identity_id' => '1070624055',
                            'gender' => 'Male',
                            'otp' => '2345',
                            'phone' => '966532042041',
                            'date_of_birth' =>'1410-03-29',
                        ];
                        break;

                    case "966541112222":
                        $data_otp_kyc =   [
                            'first_name' => 'Muna',
                            'last_name' => 'Suliman',
                            'identity_id' => '2777711111',
                            'gender' => 'female',
                            'otp' => '9990',
                            'phone' => '966541112222',
                            'date_of_birth' =>'1988-12-10',
                        ];
                        break;

                    default:

                        $data_otp_kyc = '';


                }
                return $data_otp_kyc ;
        }

        /* just for demo to return simulation result */


        $parameter = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'x-api-token' => config('absher.x_api_token')
            ],
            'json' => ['Data']
        ];

        return $this->Api()->setEndpoint($this->endpoint['payment'])->setOptions($parameter)->get();
    }

    private function Api(){

        $base_url = 'https://bnpls.free.beeceptor.com/'; //for example payments API

        return new BaseGateway( $base_url , $this->debug , $this->timeOut );
    }


    final public static function object($debug = true , $timeOut = 30){

        return new self( $debug , $timeOut );
    }
}
