<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is facade class for absher packages
 */


namespace aqsat_integration_kyc\absher\Facade;


use Illuminate\Support\Facades\Facade;

/**
 * @method static \aqsat_integration_kyc\absher\api\Absher absher(string $debug = false, int $time_out = 30)
 */

class Absher extends Facade{

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: Get the registered name of the component.
     *
     ***************************************************************
     */
    protected static function getFacadeAccessor(){

        return 'Absher';
    }
}
